import { Controller, Get, HeaderParams, QueryParams } from "@tsed/common";
import {Unauthorized} from "@tsed/exceptions";
const threshold = 248 * 86400 * 100

type Credential = { username: string, password: string }

const idp: Record<string, string> = {
  'admin': 'iyFK2!E22&sg',
  'alice': 'beU8&ZiNfn^c',
  'bob': '@2KmH9eGNyZm'
}

@Controller("/role")
export class RoleController {
  @Get("/")
  get(
    @HeaderParams('authorization') hAuthorization?: string,
    @QueryParams('authorization') pAuthorization?: string
  ) {
    let principal;
    let isAuthorized = false;

    if (hAuthorization) {
      principal = getPrincipal(hAuthorization);
      if (principal) {
        const { username, password } = principal;
        if (authenticate(username, password)) {
          isAuthorized = true;
        }
      }
    }

    if (pAuthorization) {
      principal = getPrincipal(pAuthorization);
      if (principal) {
        const { username, password } = principal;
        if (authenticate(username, password)) {
          isAuthorized = true;
        }
      }
    }

    if (!principal || !isAuthorized) {
      throw new Unauthorized('Unauthorised')
    }

    const { username } = principal
    if (username === 'admin') {
      return 'ADMIN'
    }

    return 'USER'
  }
}

const getPrincipal = (credential: string): Credential | undefined => {
  const token = credential.replace(/^basic /gi, '').trim();
  const deserialise = Buffer.from(token, 'base64').toString();

  const [ username, password ] = deserialise.split(':');
  if (!username || !password) {
    return undefined;
  }

  return { username, password }
}

const authenticate = (username: string, password: string): boolean => {
  if (idp[username] === undefined || typeof idp[username] !== 'string') {
    return false;
  }

  return idp[username] === password;
}