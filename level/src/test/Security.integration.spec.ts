import { PlatformTest } from "@tsed/common";
import SuperTest from "supertest";
import { RoleController } from "../controllers/Role";
import { Server } from "../Server";

describe("security", () => {
  let request: SuperTest.SuperTest<SuperTest.Test>;

  beforeEach(PlatformTest.bootstrap(Server, {
    mount: {
      "/": [RoleController]
    }
  }));
  beforeEach(() => {
    request = SuperTest(PlatformTest.callback());
  });

  afterEach(PlatformTest.reset);

  it("should call GET /role for Alice and return 'USER' given valid authorisation header and invalid admin authorisation parameter", async () => {
    const aliceCredential = Buffer.from("alice:beU8&ZiNfn^c").toString('base64')
    const adminCredential = Buffer.from("admin:not-valid").toString('base64')

    const response = await request.get("/role")
    .set('authorization', `basic ${aliceCredential}`)
    .query({'authorization': adminCredential})
    .expect(200);
    
    expect(response.text).toContain("USER");
 });
});
