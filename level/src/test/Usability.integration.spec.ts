import { PlatformTest } from "@tsed/common";
import SuperTest from "supertest";
import { Server } from "../Server";

describe("usability", () => {
   let request: SuperTest.SuperTest<SuperTest.Test>;

   beforeEach(PlatformTest.bootstrap(Server));
   beforeEach(() => {
      request = SuperTest(PlatformTest.callback());
   });

   afterEach(PlatformTest.reset);

   it("should call GET /role for Alice and return 'USER' given valid authorisation header", async () => {
      const credential = Buffer.from("alice:beU8&ZiNfn^c").toString('base64')

      const response = await request.get("/role")
      .set('authorization', `basic ${credential}`)
      .expect(200);
      
      expect(response.text).toContain("USER");
   });

   it("should call GET /role for Alice and return 'USER' given valid authorisation paramater", async () => {
      const credential = Buffer.from("alice:beU8&ZiNfn^c").toString('base64')

      const response = await request.get("/role")
      .query({'authorization': credential})
      .expect(200);
      
      expect(response.text).toContain("USER");
   });

   it("should call GET /role for Alice and return 401 Unauthorised incorrect password", async () => {
      const credential = Buffer.from("alice:incorrect-password").toString('base64')

      await request.get("/role")
      .set('authorization', `basic ${credential}`)
      .expect(401);
   });

   it("should call GET /role for Admin and return 'ADMIN' given valid authorisation header", async () => {
      const credential = Buffer.from("admin:iyFK2!E22&sg").toString('base64')

      const response = await request.get("/role")
      .set('authorization', `basic ${credential}`)
      .expect(200);
      
      expect(response.text).toContain("ADMIN");
   });

   it("should call GET /role for Charlie and return 401 Unauthorised given user does not exist", async () => {
      const credential = Buffer.from("charlie:^xV^spi6dpYG").toString('base64')

      await request.get("/role")
      .set('authorization', `basic ${credential}`)
      .expect(401);
   });
});
